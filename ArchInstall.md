# ==DO NOT USE - NEEDS UPDATE==
# Overview
## Software
  - Distro: Arch Linux
  - Firmware Interface: UEFI
  - Filesystem: btrfs *with subvolumes*
  - Bootmanager: either Refind or none at all ;o
  - Secure Boot: enabled
	  *The system is setup in a way which supports secure boot (which i generally recommend), but if you don't want it, you can just keep it disabled in the UEFI - the setup below should work either way* 

This guide should work both for installing alongside windows in a dual boot setup, and also if you just want to install Arch by itself.
Sections that are only needed if you just want to install Arch are marked with **Arch only**, while sections containing stuff that is only necessary for the installation with windows are marked with **Windows only**.

## Partitioning
![Partitions of the installation](assets/partitions.png)
*Your drive won't necessarily be name `sda` - e.g. `nvme0n1` if you have a nvme*

This is the partitioning we will use. The boot and Windows partition gets created when installing Windows.We're just going to shrink the main Windows partition (C:) and create a new partition that we can use for Linux. We will also use the already existing boot partition from Windows for Linux. That way dual-booting is relatively simple.

If you want to wipe the disk and only install Arch, then you also need to create the boot partition. In this scenario the Windows partition doesn't exists and the Linux partition is the second one.

In the Linux partition you can also see the different subvolumes. Subvolumes are a btrfs feature which allows you to create indepentend 'sections' on a partition which act a little bit like separate partitions.
But with subvolumes you can do much more. We will be using them mainly to potentially backup our installation with snapshots, which are copies of a subvolume which can be created instantly while the system is running. 

# Installation
For the actual installation you will need:
  - a Computer with Windows already installed
  - a USB stick for Arch and gparted *(you can use [Ventoy](https://www.ventoy.net/en/index.html) to just place the iso files on the USB an then boot from it)*
  - a little bit time ;)

## Prepare drive *(Windows only)*
If you already have free space on your drive you can skip to [Boot installation environment](#boot-installation-environment).

For editing our partitions we will use gparted, because it's easy.

  1. Download the ISO from [sourceforge.net](https://sourceforge.net/projects/gparted/files/gparted-live-stable/1.4.0-1/gparted-live-1.4.0-1-amd64.iso/").
  2. Drop the ISO to the USB with ventoy installed.
  3. Boot the live USB with gparted.
  
![GParted with all windows partitions](assets/gparted-first.png)  
In gparted you should see all the partitions from your SSD.

In this case:
  - **`sda1`** is the boot partition
  - **`sda2`** is a weird Microsoft partition *(Microsoft Reserved Partition)*
  - **`sda3`** is the main Windows partition
  - **`sda4`** is another weird Microsoft partition *(Recovery Partition)*

Now our goal is to make a new partition for Linux.
  1. First shrink the big main Windows partition (`sda3`), with a click on the Resize/Move Button, that you have space for the Linux one.
  2. Now you should have unallocated space at the end of the drive.
  3. If you also have the additional Windows recovery partition you can either move it from the end to the beginning of the free section or just delete it.
  4. You can apply these operation with a click on the green checkmark.

![GParted with windows partition shrinked](assets/gparted-second.png)

---
## Delete secure boot keys in UEFI (kinda optional)
To later easily enroll the generated secure boot keys, you should go into your UEFI (usually by pressing `{DEL}` or something else (pls google) during booting) and delete your current secure boot keys.
This is done differently in every UEFI so I cannot give clear instructions for that. 
But the options should usually be under the tab "Security" or "Boot".
The option could also be called something like "Reset to setup mode" or something like that.
If there's an indicator which previously said "User mode" this should say "Setup mode".

If this doesn't work for some reason, just disable secure boot and proceed with the next step. You don't need to enroll the keys (right now) to be able to boot your installation.

## Boot-Installation-Environment
You now need to boot the Arch installation image (e.g. via the boot menu).

When it has booted, first select your keymap (in my case `de`):
~~~ shell
loadkeys de 
~~~

Then verify that you are in uefi mode - when it shows many lines it's ok
~~~ shell
efivar -l
~~~

Make sure you have plugged in ethernet or connect to wifi:

**Connect to wifi network**
  1. `iwctl`
  2. `station list` - list all wifi-adapters, e.g. *wlan0*
  3. `station wlan0 get-networks` - get all networks
  4. `station wlan0 connect {wifi-name}` - connect to network
  5. enter password
  6. `exit` when successfully connected

Verify that you can access the internet:
~~~
ping 1.1.1.1
~~~

Ensure that the time is correct:
~~~~~~~~~~~~~~~~~~~~~~~~~~~ shell
timedatectl set-ntp true
~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Enable SSH access *(optional)*
If you want to just copy paste commands from the guide, you can enable ssh login in the arch installation environment.
For that you just have to set a password for the default root user (this can be anything e.g. `1234` – it's only temporary):
~~~ shell
passwd
~~~

Now you can login via ssh from another computer on your local network:
~~~shell
ssh root@{ip}
~~~
*Note: You can get the local ip of the arch installation system with `ip addr show`; there should be a network card like `enp0s3` and after `inet` is the local ip*

---

## Partition drive
List all drives and partitions with `lsblk`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
NAME       MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
loop0        7:0    0 666.6M  1 loop /run/archiso/airootfs
sda          8:0    0 238.5G  0 disk 
├─sda1       8:1    0   100M  0 part 
├─sda2       8:2    0    16M  0 part 
└─sda3       8:3    0   201G  0 part
sdb          8:16   1  57.7G  0 disk
├─sdb1       8:17   1  57.6G  0 part
│ └─ventoy 254:0    0 786.3M  1 dm   /run/archiso/bootmnt
└─sdb2       8:18   1    32M  0 part  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Identify the drive on which you want to install Arch.
e.g. `/dev/sda`

For example with an existing windows installation:
- **`sda`** is the SSD with ~240GB
- **`sda1`** is the boot partition
- **`sda2`** is the microsoft reserved thingy
- and **`sda3`** the windows partition (now also smaller than our SSD => we have space for linux)

To partition it for Arch, use gdisk
~~~shell
gdisk /dev/sda
~~~

In gdisk every action is access with a letter that you have to type and then press enter to execute it. For example to list all partitions write `p` then `{Enter}`. 

### Delete existing stuff *(Arch only)*
If you already have partitions on that particular drive that you want to delete *(you can list partitions with `p`)* 
- `x` to access the advances functionality 
- `z` to destroy the existing partition table.
- then `Y` two times

Then enter gdisk again (with `gdisk /dev/sda`).

### Create boot partition *(Arch only)*
The computer needs a special partition on the drive from which the OS can boot.
If you didn't delete all partitions and you already have a boot partition, e.g. from Windows, you don't need to do this.

- `n` to create a new partiton 
- `Enter` to accept default partition number (should be 1)
- `Enter` to use default starting point
- `+512M` to make the partition 512MB big
- `EF00` to set the partition type to EFI system partition 

### Create Linux partition
When listing the current partitions with `p`, there should be either just a boot partition or a boot partition and one (or maybe more) windows partition. The important thing is, that we have free space to create a new partition for Arch.

- `n` to create a new partition
- *`Enter`* to use default partition number *(maybe remember that for changing its name later)*
- *`Enter`* to use default start point (beginning of free space)
- *`Enter`* to use default end point (end of free space)
- *`Enter`* to use default type (Linux filesystem)
- `p` check everything (there should be a new partition)

**Change name of partition**
- `c`
- *`4`* number of the arch partition *if unsure check with `p`* 
- *`system`* name of the partition (can be anything)

### Write to disk
- `p` check again if everythin is correct
- `w` to write changes
- `Y`

---

## Filesystem
We will use btrfs as file system for our main Linux partition.

Our file system then will have different subvolumes so we can easily backup them.

### Create Boot partition file system *(Arch only)*
This step is only needed if you newly created the boot partition. If there already was one, for example due to windows, don't execute the following command.

The boot partition has to be formatted with FAT32:
``` shell
mkfs.fat -F 32 /dev/sda1
```
*Note: replace sda1 with the identifier of the boot partition; can be obtained with `lsblk` - should be the 512M big partition*

### Label boot partition file system
To easier identify the boot partition fs we can label it.

In our case `/dev/sda1` is the boot partition. `EFI` is the name.
~~~~~~~~~~~~~~~~~~ Shell
fatlabel /dev/sda1 EFI
~~~~~~~~~~~~~~~~~~

### Create-main-file-system
~~~~~~~~~~~~~~~~~~ bash
mkfs.btrfs /dev/disk/by-partlabel/system -L system
~~~~~~~~~~~~~~~~~~
`-L system` label the filesystem (`system` is the name, can be anything)

*Note: replace `system` in `/dev/disk/by-partlabel/system` with whatever name you gave your partition earlier. You can also use `/dev/sda4` (or whatever your linux partition is)*

If you get an error about an already existing file system you can add `-f` to that command to overwrite it, if you'r sure it's the right partition.   

### Create subvolumes
We will create following subvolumes:
  - **`@`** our main linux partiton (root) \[will be backed up]
  - **`@home`** the directory in which all user files are \[will be backed up]
  - **`@swap`** on which we create a swapfile (if necessary)
  - **`@snapshots`** place to store snapshots (=backups)

Those will be created at the root of our btrfs file system and later mounted at the correct location. 

*Note: the @ is just a convention to distinguish subvolumes from normal directories*

To create the subvolumes we first have to mount the fs
~~~~~~~~~~~~~~~~~~ shell
mount /dev/disk/by-label/system /mnt
cd /mnt
~~~~~~~~~~~~~~~~~~
*From now on I will access the main partition through its file system label (`by-label/system`) that was set when [creating the file system](#create-main-file-system)*

Repeat the following line for every subvolume:
~~~~~~~~~~~~~~~~~~ bash
btrfs subvolume create @
btrfs subvolume create @home
btrfs subvolume create @swap
btrfs subvolume create @snapshots
~~~~~~~~~~~~~~~~~~

You can use `ls` to check if all subvolumes have been created

When you are finished unmount the main btrfs filesystem:
~~~~~~~~~~~~~~~~~~ bash
cd
umount /mnt
~~~~~~~~~~~~~~~~~~

---

## Mounting everything important
We are now finished with setting up our drive, but to begin with our installation we have to mount everything in the right place.

We will install our system to `/mnt` - so our future root (`/`) has to be mounted there.

**Root**
~~~~~~~~~~~~~~~~~~ bash
mount /dev/disk/by-label/system /mnt -o subvol=@
~~~~~~~~~~~~~~~~~~
`-o subvol=@` specifies that we don't want to mount just the btrfs partition but the subvolume `@` which we created earlier

We then have to create some folders in our future root:
~~~~~~~~~~~~~~~~~~ bash
mkdir /mnt/{home,efi,swap,snapshots}
~~~~~~~~~~~~~~~~~~

**Boot**
Mount the boot partition (we can access the partition with its FS-Label (`EFI`) that we gave it earlier)
~~~~~~~~~~~~~~~~~~ bash
mount /dev/disk/by-label/EFI /mnt/efi
~~~~~~~~~~~~~~~~~~

**Home, swap & snapshots**
~~~~~~~~~~~~~~~~~~ bash
mount /dev/disk/by-label/system /mnt/home -o subvol=@home
mount /dev/disk/by-label/system /mnt/swap -o subvol=@swap
mount /dev/disk/by-label/system /mnt/snapshots -o subvol=@snapshots
~~~~~~~~~~~~~~~~~~

You can do `lsblk` to see all mountpoints.

---

## Create swap (optional)
Swap is used to copy data from RAM to the disk to free up RAM space. Recommended if you don't have a large amount of RAM.
It's also necessary if you want the ability to hibernate.
For hibernation the swap must me at least as big as the RAM so it's recommended to make it the same size.

We have mounted a subvolume for our swapfile.

To create a swapfile use the following command:
~~~~~~~~~~~~~~~~~~ bash
btrfs filesystem mkswapfile --size 16G --uuid clear /mnt/swap/swapfile
~~~~~~~~~~~~~~~~~~
  - `size` specifies the size of the swapfile (Usually as big as your RAM)

Activate it:
~~~~~~~~~~~~~~~~~~ bash
swapon /mnt/swap/swapfile
~~~~~~~~~~~~~~~~~~

---

## Download and install arch
//TODO specify that you have to chose amd or intel ucode before hitting enter
~~~~~~~~~~~~~~~~~~ bash
pacstrap /mnt base linux-zen linux-zen-headers linux-firmware base-devel btrfs-progs vim nano dhcpcd fish git efitools {amd-ucode or intel-ucode}
~~~~~~~~~~~~~~~~~~
You can specify any package you want to be installed

  - `linux-zen` is the kernel itself (the normal one would be linux, but i like to use zen)
  - `linux-zen-headers`
  - `linux-firmware` most drivers
  - `base-devel` developer tools
  - `btrfs-progs` for btrfs stuff
  - `vim`/`nano` text editor
  - `dhcpcd` to get a dhcp ip address //todo
  - `fish` shell i like to use (alternative to bash)
  - `efitools` needed for secureboot i guess
  - `amd-ucode` or `intel-ucode` (depending on CPU brand) - provides security and stability updates to the processors [microcode](https://en.wikipedia.org/wiki/Microcode) *normally you also have to load it somehow but `sbctl` takes care of that*

To automatically mount all the partitions later exactly as they are mounted now run: //todo better description
~~~~~~~~~~~~~~~~~~ bash
genfstab -L /mnt >> /mnt/etc/fstab
~~~~~~~~~~~~~~~~~~

---

## Configure system
Now we installed arch, but we have to do a few things before trying to boot

### Enter system
We can go into the system without rebooting
~~~~~~~~~~~~~~~~~~ bash
arch-chroot /mnt
~~~~~~~~~~~~~~~~~~

You can now also use fish as a shell, which should be easier to use. Just type `fish` then `{enter}` 

### Misc. Configuration
#### Timezone
~~~~~~~~~~~~~~~~~~ bash
ln -sf /usr/share/zoneinfo/Europe/Vienna /etc/localtime
~~~~~~~~~~~~~~~~~~

#### Sync system clock with hardware clock
~~~~~~~~~~~~~~~~~~ bash
hwclock --systohc
~~~~~~~~~~~~~~~~~~

#### Generate and set locales
Uncomment (remove `#`) **`en_US.UTF-8`** & **`de_AT.UTF-8`** in the `locale.gen` file 
~~~~~~~~~~~~~~~~~~ bash
nano /etc/locale.gen
~~~~~~~~~~~~~~~~~~
 *Note: You can search with `{Ctrl/Strg} + W` in nano; you can also use vim if that's your thing*

**Generate the locales:**
~~~~~~~~~~~~~~~~~~ bash
locale-gen
~~~~~~~~~~~~~~~~~~

**Set the locale:**
Insert: `LANG=en_US.UTF-8` (or the language you want to use) in `locale.conf`
~~~~~~~~~~~~~~~~~~ bash
nano /etc/locale.conf
~~~~~~~~~~~~~~~~~~

#### Set keymap
Insert: `KEYMAP=de` in `vconsole.conf`
~~~~~~~~~~~~~~~~~~ bash
nano /etc/vconsole.conf
~~~~~~~~~~~~~~~~~~

#### Set hostname
Insert only the name of the computer
~~~~~~~~~~~~~~~~~~ bash
nano /etc/hostname
~~~~~~~~~~~~~~~~~~

#### Pacman
Configure pacman to use fancier output and parallel downloads which speed up downloading stuff.
~~~
nano /etc/pacman.conf
~~~
Then uncomment the following lines:
~~~
Color
VerbosePkgLists
ParallelDownloads = 5
~~~
*You can also add `ILoveCandy` below the parallel downloads line to enable fancier progress bars ;)*
### Users
**Set root password**
~~~~~~~~~~~~~~~~~~ bash
passwd
~~~~~~~~~~~~~~~~~~
Then enter a new password for the root user

#### Create new user
~~~~~~~~~~~~~~~~~~ bash
useradd -m -G wheel -s /usr/bin/fish {username}
~~~~~~~~~~~~~~~~~~

**Set password**
~~~~~~~~~~~~~~~~~~ bash
passwd {username}
~~~~~~~~~~~~~~~~~~

**Enable sudo commands for every user in group wheel**
Remove `#` from `%wheel ALL=(ALL:ALL) ALL`

~~~~~~~~~~~~~~~~~~ bash
EDITOR=nano visudo
~~~~~~~~~~~~~~~~~~
*By default the command `visudo` uses vi as an editor; to specify that we want to use nano, we set the environment variable `EDITOR` to the desired editor*

### Install yay (kinda optional)
You actually don't necessarily need `yay` for anything in this guide, but i just like it as my package manager

~~~~~~~~~~~~~~~~~~ bash
su {username}
cd #note: changes in the home directory of the user, as we don't have the permission to create a folder otherwise

git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin

makepkg -sic

exit
~~~~~~~~~~~~~~~~~~

### Setup system to boot
#### Configure initial ramdisk
The initial ramdisk is a very small environment which gets executed at boot to load various kernel modules and sets up things before actually starting the system.

We have to add/edit a few things that should be in this image
~~~~~~~~~~~~~~~~~~ bash
nano /etc/mkinitcpio.conf
~~~~~~~~~~~~~~~~~~

Edit the line starting with `HOOKS=` (the one without `#` in front of it) to this:
~~~~~~~~~~~~~~~~~~ bash
HOOKS=(systemd autodetect modconf block filesystems)
~~~~~~~~~~~~~~~~~~
Explanation:
  - `systemd` - the default install uses busybox init, we use systemd so this is the hook needed
  - `autodetect` - shrinks the initramfs by only including the truly necessary things from the hooks after it
  - `block` - stuff needed for sata, usb, etc. (= block devices)
  - `filesystems` necessary file system modules

Generate the ramdisk:
~~~~~~~~~~~~~~~~~~ bash
mkinitcpio -P
~~~~~~~~~~~~~~~~~~

#### Installing sbctl
We will use `sbctl` for most things related to booting and secure boot.
~~~ shell
pacman -S sbctl
~~~

#### Generating secure boot keys
~~~ shell
sbctl create-keys
~~~

#### Enrolling secure boot keys
This will add the generated secure boot keys to the UEFI.
This will only work if your system is in setup mode. (check with `sbctl status`)
If this doesn't work, you should be able to skip this step and enroll the keys later.
~~~ shell
sudo sbctl enroll-keys 
~~~

==WARNING: needs testing==
Or if you also want to boot Windows (or the first command errors for a reason):
~~~ shell
sudo sbctl enroll-keys -m
~~~

#### UKI
We will boot our system using a unified kernel image (UKI) which is basically a single efi-executable which contains all information to boot up Linux. It has basically two advantages in our case:
1. It can be directly bootet from the UEFI without the need of a separate boot loader
2. You just need to sign this single file to be relatively safe – for example the kernel command line can't be set externally as it's already contained in the image

The generation and signing of the UKI can also be automated with `sbctl`.

##### Kernel command line
The command line can be used to change parameter of the linux kernel itself. For example we need to specify the root partition on which the main file system is.

In order for `sbctl` to set the command line in the UKI you need to write it to a file:
~~~ shell
nano /etc/kernel/cmdline
~~~

Insert: `root=LABEL=system rootflags=subvol=@ rw`
*Note: You need to change `system` to whatever name you gave to your file system*
##### Creating bundle
No we need to create a "bundle" to tell `sbctl` where do save the UKI.

Create folder in which the bundle will be saved:
~~~ shell
mkdir /efi/EFI/Arch -p
~~~

The following command will create the bundle. You will probably need to adjust its arguments a little bit (read explanation before running):
~~~ shell
sudo sbctl bundle --save --splash-img /usr/share/systemd/bootctl/splash-arch.bmp --initramfs /boot/initramfs-linux-zen.img --kernel-img /boot/vmlinuz-linux-zen --intelucode /boot/intel-ucode.img /efi/EFI/Arch/linux-zen-signed.efi 
~~~

- **`--splash-img`**: specifies which image to show at boot
- **`--initramfs`** & **`--kernel-img`**: if you're not using `linux-zen` you need to change this; if you run the default `linux` kernel, this option can be omitted *\**
-  **`--intelucode`** specifies CPU microcode; use **`--amducode /boot/amd-ucode.img`** if you have AMD; if you don't have microcode either install the package `amd-ucode` or `intel-ucode`, or leave out the option if you don't want to use it
- **`/efi/EFI/Arch/linux-zen-signed.efi`**: the location to where the bundle gets written; replace this with the name you already have on your boot partition (e.g. just `linux-signed.efi` if you use the default kernel)

If you have multiple kernels you can also repeat the command with different settings for each kernel you want.

You also need to sign that bundle:
~~~ shell
sbctl sign /efi/EFI/Arch/linux-zen-signed.efi
~~~
*Note: replace `/efi/EFI/Arch/linux-zen-signed.efi` with the location of the bundle if you changed it*

#### Bootloader
I generally recommend `refind` as a bootloader. It automatically scans for the images generated by `sbupdate` and also for existing windows installations, so you don't have to configure anything.
Additionally, when you boot with a bootable USB-drive plugged in, `refind` also automatically shows it in its menu.

But if you just want to boot into arch and don't really plan on booting something else very often, you don't really need a boot loader, which is probably the best option.

**Chose one:**
##### Refind
**Downlaod refind package:**
~~~ shell
pacman -S refind
~~~

Install it:
~~~ shell
refind-install
~~~

Refind also needs to be signed if you want to boot with secure boot.
~~~~~~~~~~~ bash
sbctl sign -s /efi/EFI/refind/refind_x64.efi
~~~~~~~~~~~

##### Directly boot images (if you don't use refind)
As stated earlier `sbupdate` creates efi-executables, which can be loaded by the uefi directly. But for that you have to create an entry which points to the created image.

For that we need `efibootmgr`
~~~ shell
pacman -S efibootmgr
~~~

Now we can create the boot entry:
~~~ shell
efibootmgr --create --label "Arch Linux" --loader 'EFI\\Arch\\linux-zen-signed.efi' --unicode
~~~
  - `EFI/ARCH/linux-zen-signed.efi` - is the path to the boot image that will be created by `sbupdate`

*Note: `efibootmgr` should detect the boot partition by itself 
if that doesn't work for some reason, you can specify the drive and the partition number with `--disk` & `--part`* 
	*e.g.: `efibootmgr --disk /dev/sda --part 1 {...rest of the cmd above}`*

After this command `efibootmgr` should print a list with all available boot entries which should contain for example network boot or usb boot thingies, but also your newly created "Arch Linux" entry.

This entry can then be set as the first thing the computer boots in the boot order settings in your UEFI (BIOS).

### Exit
That should be everything we have to do in the live system.

Run `exit` until you are in the arch installation environment.
If you had configured swap: `swapoff /mnt/swap/swapfile`
`umount /mnt -R` to unmount everything.  
Then `poweroff` to shut down your PC.


# Yayy
Your system should now boot into either `Refind` where you can choose `Arch` or `Windows` or directly into `Arch`

If your system doesn't boot into `Refind` or `Arch`, but into `Windows` for example, change the boot order in your UEFI-Settings.

You now have a working minimal installation of `Arch Linux`, that you can play around with and install stuff.
Important tip: We didn't really configure the network interface. So if you use DHCP (which you probably do) you have to execute `sudo dhcpcd` in the terminal to get an IP or else you aren't able to download anything.

To configure the network properly or to install a desktop environment read my other guide.

[](Plasma.md)
