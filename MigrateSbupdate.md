In my old guide I used the tool `sbupdate` to generate and sign a [unified kernel image](https://wiki.archlinux.org/title/Unified_kernel_image) from which you then can boot. This tool is now deprecated and should be no longer used – there were also problems with it lately.

This document will outline the necessary steps to migrate an existing installation created with my old guide to the new `sbctl` tool.

`sbctl` is a relatively new tool which essentially is capable of doing the same things – and it's also in the official Arch repos.

# Installing sbctl

~~~ shell
yay -S sbctl
~~~

# Importing existing keys
`sbctl` is able to generate secure boot keys and also import them into EFI. But since we already have keys and they're also already enrolled we will use them.

To import the existing keys you still have to create new ones because it doesn't allow you to import them otherwise.
~~~ shell
sudo sbctl create-keys
~~~

Now we will import the existing ones and overwrite the ones we created.
~~~ shell
sudo sbctl import-keys --pk-cert /etc/efi-keys/PK.crt --pk-key /etc/efi-keys/PK.key --kek-cert /etc/efi-keys/KEK.crt --kek-key /etc/efi-keys/KEK.key --db-cert /etc/efi-keys/DB.crt --db-key /etc/efi-keys/DB.key
~~~

# Creating UKI
Next we have to tell `sbctl` to create unified kernel images (UKIs) which previously `sbupdate` did. 

## Kernel command line
The UKI contains the kernel command line which is used to set basic configuration for booting the kernel.

To use the same command line that `sbupdate` used, look at its configuration:
~~~ shell
cat /etc/sbupdate.conf
~~~
In this file there should be a variable called `CMDLINE_DEFAULT`.
e.g.: `CMDLINE_DEFAULT="root=LABEL=system rootflags=subvol=@ rw"`

Insert the value between the double quotes (but without them) in a new file:
~~~ shell
sudo nano /etc/kernel/cmdline
~~~

`sbclt` will now use this value.

## Creating bundle
List the kernel images you currently have on your boot partition:
~~~ shell
sudo ls /efi/EFI/Arch/
~~~
There should be minimum one file with a `.efi` extension. E.g.: `linux-zen-signed.efi`

The name should indicate which kernel you are running. In the above example `linux-zen` which is not the default Arch one. If you instead have a file named like `linux-signed.efi` you have the default `linux` kernel. 

The following command will create the bundle. You will probably adjust its arguments a little bit (read explanation before running):
~~~ shell
sudo sbctl bundle --save --splash-img /usr/share/systemd/bootctl/splash-arch.bmp --initramfs /boot/initramfs-linux-zen.img --kernel-img /boot/vmlinuz-linux-zen --intelucode /boot/intel-ucode.img /efi/EFI/Arch/linux-zen-signed.efi 
~~~

- **`--splash-img`**: specifies which image to show at boot
- **`--initramfs`** & **`--kernel-img`**: if you're not using `linux-zen` you need to change this; if you run the default `linux` kernel, this option can be omitted *\**
-  **`--intelucode`** specifies CPU microcode; use **`--amducode boot/amd-ucode.img`** if you have AMD; if you don't have microcode either install the package `amd-ucode` or `intel-ucode`, or leave out the option if you don't want to use it
- **`/efi/EFI/Arch/linux-zen-signed.efi`**: the location to where the bundle gets written; replace this with the name you already have on your boot partition (e.g. just `linux-signed.efi` if you use the default kernel)

If you have multiple kernels you can also repeat the command with different settings for each kernel you want.

# Sign everything
`sbctl` has our existing keys and we also wrote a bundle but we haven't signed it yet.

Sign it with:
~~~ shell
sudo sbctl sign /efi/EFI/Arch/linux-zen-signed.efi
~~~
Replace the path with the path of the bundle(s) you created above.

## Refind (only if you use refind)
If you use `refind`, the funny boot menu which allows you to select the thing to boot, you also have to sign that.
But before signing you also have to reinstall `refind` because the existing files are already signed.
~~~ shell
sudo refind-install
sudo sbctl sign /efi/EFI/refind/refind_x64.efi
~~~

# Check everything
To list all bundles:
~~~ shell
sudo sbctl list-bundles
~~~
It should indicate that the bundles are signed.

To list all files on the boot partition and their signing status:
~~~ shell
sudo sbctl verify
~~~
You should see that the `refind` file is signed (if installed). If you think you need to sign other files as well you can do that with `sudo sbctl sign`.

You should now be able to successfully reboot your system.
If everything worked, you can now also uninstall sbupdate.
~~~ shell
yay -Rns sbupdate-git
~~~