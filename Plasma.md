This document will explain how to install and setup KDE Plasma as a DE and also explain basic stuff.
You will need a [basic Arch installation](ArchInstall.md) and should be able to boot and login with the created user.

# Networking
Before we can start we need a internet connection to download packages. If you try to ping something you will notice, that there is none.

**Wired**
If the computer is connected over ethernet it probably only needs an IP, DNS and default gateway. Those things usually come from a DHCP-Server in your network (probably your router).

Later when plasma is installed NetworkManager will take care of that (you can then interact with it in the taskbar) but until then we temporarily need to get those things. To do that we installed `dhcpcd` beforehand and just need to run it with sudo.
~~~ bash
sudo dhcpcd
~~~

Now the pings should work.

**Wireless**
//TODO
# Preparation
## Enable 32 bit packages (kinda optional)
By default you cannot install 32-bit packages because the repository containing them is disabled by default. But if you want to install steam for example you have to enable it as steam is still 32 bit.

To do that open the pacman configuration file in a text editor:
~~~ shell
sudo nano /etc/pacman.conf 
~~~

Then scroll down to the line mentioning multilib and uncomment (=remove #) this and the next line, which should then look like this:
~~~
[multilib]
Include = /etc/pacman.d/mirrorlist
~~~

**Update package list**
~~~ shell
yay
~~~

# Installing stuff
## GPU Drivers
To properly utilize your GPU it's important to install the right driver. As this is a little bit complicated the following installation instructions are very basic and **only** cover systems with relatively new AMD, NVIDIA or Intel (integrated) GPUs. It doesn't account for [hybrid graphics](https://wiki.archlinux.org/title/Hybrid_graphics) (e.g. in Laptops Intel CPU + dedicated Nvidia GPUs). For those setups you have to do a bit of searching yourself – sorry.

### AMD
~~~ shell
yay -S mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon
~~~
- `mesa` is the main driver thingy (`lib32-mesa` the 32-bit variant | only available if multilib is enabled)
- `vulkan-radeon` the open source vulkan driver (`lib32-vulkan-radeon` the 32-bit variant)

[More information](https://wiki.archlinux.org/title/AMDGPU)

### Intel
~~~ shell
yay -S mesa lib32-mesa vulkan-intel lib32-vulkan-intel
~~~
- `mesa` is the main driver thingy (`lib32-mesa` the 32-bit variant | only available if multilib is enabled)
- `vulkan-intel` the open source vulkan driver (`lib32-vulkan-intel` the 32-bit variant)

[More information](https://wiki.archlinux.org/title/Intel_graphics)

### NVIDIA 🤢
~~~ shell
yay -S nvidia-dkms lib32-nvidia-utils 
~~~
- `nvidia-dkms` is the proprietary driver from NVIDIA
- `lib32-nvidia-utils` is needed for 32-bit support; only available if multilib is enabled
*Note: The `dkms` variant is needed as I used a custom linux kernel (`linux-zen`) in the last guide. If you have the normal arch linux kernel (`linux`) you can also install the `nvidia` package.*

[More information](https://wiki.archlinux.org/title/NVIDIA)

## Pipewire
`Pipewire` is a multimedia framework which we use mainly for audio (and also video) stuff. By default when you install Plasma it will use `pulseaudio` which is the "old" audio system. To use `pipewire` we install it explicitly.

~~~ shell
yay -S pipewire pipewire-pulse pipewire-jack wireplumber 
~~~

## Plasma
To install Plasma itself there's a package group named `plasma` which contains all packages needed for running the most basic  
~~~ shell
yay -S plasma plasma-wayland-session konsole dolphin
~~~

# Enabling services
~~~
sudo systemctl enable NetworkManager sddm
~~~

# Configuration
To specify the keyboard layout for `sddm`, set it via the following command:
``` bash
sudo localectl set-x11-keymap de
```
