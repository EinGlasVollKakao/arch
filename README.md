Repo containing tutorials for Arch linux

- [Install Arch](ArchInstall.md)
- [Installing basics stuff and Plasma](Plasma.md)

Important if you had installed Arch with an old version of the guide:
- [Migrating from sbupdate to sbctl](MigrateSbupdate.md)
